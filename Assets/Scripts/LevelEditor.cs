﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditor : MonoBehaviour
{
    public GameObject tilePrefab;

    [Header("Tile")]
    public Sprite tileSprite;
    public Sprite tileYellowSprite;
    public Sprite tileGreenSprite;
    public Sprite tileBlueSprite;
    public Sprite tileRedSprite;
    public Sprite tileOrangeSprite;

    public Sprite constructionTileSprite;
    public Sprite deleteTileSprite;

    [Header("Door")]
    public Sprite doorSprite;
    public Sprite doorYellowSprite;
    public Sprite doorGreenSprite;
    public Sprite doorBlueSprite;
    public Sprite doorRedSprite;
    public Sprite doorOrangeSprite;

    [Header("Other")]
    public InputField levelInput;
    public GameObject uiPanel, loadContent;
    public Text versionText;
    public Button buttonPrefab;

    public int tileSize, cameraSpeed;

    private List<Tile> tiles = new List<Tile>();
    private List<Door> doors = new List<Door>();
    private List<Button> buttons = new List<Button>();

    private Tile tile = null;
    private Door door = null;

    public string currentLevel = "";

    private Vector2 currentPosition;

    private bool uiVisible = true;

    // Start is called before the first frame update
    private void Start()
    {
        versionText.text =  "Color Game Level Editor Version: " + Application.version;

        Tile.tileSprite = tileSprite;
        Tile.tileYellowSprite = tileYellowSprite;
        Tile.tileGreenSprite = tileGreenSprite;
        Tile.tileBlueSprite = tileBlueSprite;
        Tile.tileRedSprite = tileRedSprite;
        Tile.tileOrangeSprite = tileOrangeSprite;

        Tile.constructionTileSprite = constructionTileSprite;
        Tile.deleteTileSprite = deleteTileSprite;

        Door.doorSprite = doorSprite;
        Door.doorYellowSprite = doorYellowSprite;
        Door.doorGreenSprite = doorGreenSprite;
        Door.doorBlueSprite = doorBlueSprite;
        Door.doorRedSprite = doorRedSprite;
        Door.doorOrangeSprite = doorOrangeSprite;

        tile = new Tile(Instantiate(tilePrefab), Tile.Type.Gray);
        door = new Door(Instantiate(tilePrefab), Door.Colors.Gray, "level");

        door.gameObject.GetComponent<SpriteRenderer>().sprite = doorSprite;

        tile.gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite;

        tile.UpdateColor();

        door.gameObject.SetActive(false);

        for (int x = 0; x < 100; x++)
        {
            for (int y = 0; y > -100; y--)
            {
                GameObject gridTile = Instantiate(tilePrefab);
                gridTile.transform.position = new Vector2(x, y);
                gridTile.GetComponent<SpriteRenderer>().sprite = constructionTileSprite;
            }
        }

        Cursor.visible = true;

        CheckExistingLevels();
    }

    private bool addingDoor = false;

    private void Update()
    {
        currentPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (currentPosition.x < 0)
        {
            currentPosition.x = 0;
        }

        if (currentPosition.y > 0)
        {
            currentPosition.y = 0;
        }

        if (uiVisible)
        {
            uiPanel.SetActive(true);
            Cursor.visible = true;

            if (Input.GetKeyDown(KeyCode.Escape))
            {

#if UNITY_EDITOR
                // Application.Quit() does not work in the editor so
                // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }

            return;
        }
        else
        {
            uiPanel.SetActive(false);
            Cursor.visible = false;
        }

        tile.gameObject.transform.position = SnapToGrid(currentPosition);
        tile.gameObject.transform.position = new Vector3(tile.gameObject.transform.position.x, tile.gameObject.transform.position.y, -1);
        door.gameObject.transform.position = new Vector3(tile.gameObject.transform.position.x, tile.gameObject.transform.position.y, -1);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (addingDoor)
            {
                AddDoor();
            }
            else
            {
                AddTile();
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (!addingDoor)
            {
                tile.tileType++;

                if ((int)tile.tileType >= 7)
                {
                    addingDoor = true;
                    tile.tileType = 0;
                    door.gameObject.SetActive(true);
                    tile.gameObject.SetActive(false);
                }

                tile.UpdateColor();
            }
            else
            {
                door.doorColor++;

                if ((int)door.doorColor >= 6)
                {
                    addingDoor = false;
                    door.doorColor = 0;
                    door.gameObject.SetActive(false);
                    tile.gameObject.SetActive(true);
                }

                door.UpdateColor();
            }
        }

        if (Input.GetKey(KeyCode.A))
        {
            Camera.main.transform.position -= new Vector3(cameraSpeed, 0, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            Camera.main.transform.position += new Vector3(cameraSpeed, 0, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.W))
        {
            Camera.main.transform.position += new Vector3(0, cameraSpeed, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            Camera.main.transform.position -= new Vector3(0, cameraSpeed, 0) * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Camera.main.orthographicSize += 0.5f;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            Camera.main.orthographicSize -= 0.5f;
        }

        if (Input.GetKey(KeyCode.Return))
        {
            SaveLevel(currentLevel);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SaveLevel(currentLevel);

            foreach (Tile t in tiles)
            {
                Destroy(t.gameObject);
            }

            foreach (Door d in doors)
            {
                Destroy(d.gameObject);
            }

            tiles.Clear();
            doors.Clear();

            CheckExistingLevels();

            uiVisible = true;

        }

    }

    private void AddTile()
    {
        tile.gameObject.transform.position = new Vector3(tile.gameObject.transform.position.x, tile.gameObject.transform.position.y, 0);

        if (tile.tileType == Tile.Type.Delete)
        {
            foreach (Door d in doors)
            {
                if (d.gameObject.transform.position == tile.gameObject.transform.position)
                {
                    doors.Remove(d);
                    Destroy(d.gameObject);
                    return;
                }

            }
        }

        foreach (Tile t in tiles)
        {
            if (t.gameObject.transform.position == tile.gameObject.transform.position)
            {
                if (tile.tileType == Tile.Type.Delete)
                {
                    tiles.Remove(t);
                    Destroy(t.gameObject);
                    return;
                }

                t.tileType = tile.tileType;
                t.UpdateColor();
                return;
            }
        }

        if (tile.tileType == Tile.Type.Delete)
        {
            return;
        }

        Tile newTile = new Tile(Instantiate(tilePrefab), tile.tileType);
        newTile.gameObject.transform.position = tile.gameObject.transform.position;
        newTile.UpdateColor();
        tiles.Add(newTile);
    }

    private void AddDoor()
    {
        door.gameObject.transform.position = new Vector3(tile.gameObject.transform.position.x, tile.gameObject.transform.position.y, 0);

        foreach (Door d in doors)
        {
            if (d.gameObject.transform.position == door.gameObject.transform.position)
            {
                d.doorColor = door.doorColor;
                d.UpdateColor();
                return;
            }

        }

        Door newDoor = new Door(Instantiate(tilePrefab), door.doorColor, door.levelToLoad);
        newDoor.gameObject.transform.position = door.gameObject.transform.position;
        newDoor.UpdateColor();

        doors.Add(newDoor);
    }

    private Vector2 SnapToGrid(Vector2 position)
    {
        if (position.x % tileSize != 0)
        {
            position.x = Mathf.Floor(position.x);
        }

        if (position.y % tileSize != 0)
        {
            position.y = Mathf.Floor(position.y);
        }

        return position;
    }

    public void SaveLevel(string fileName)
    {
        using (TextWriter tw = new StreamWriter(directory + fileName + ".txt"))
        {
            foreach (Tile t in tiles)
            {
                //Debug.Log(t.gameObject.transform.position.x + " " + t.gameObject.transform.position.y);
                tw.WriteLine(t.ToString());
            }

            foreach (Door d in doors)
            {
                tw.WriteLine(d.ToString());
            }
        }
    }

    private string directory = "Levels/";

    public bool LoadLevel(string fileName)
    {
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }


        if (!File.Exists(directory + fileName + ".txt"))
        {
            return false;
        }

        tiles.Clear();

        string[] data;
        foreach (string line in File.ReadLines(directory + fileName + ".txt"))
        {
            data = line.Split('|');

            if (data[0] == "D")
            {
                Door newDoor = new Door(Instantiate(tilePrefab), (Door.Colors)Enum.ToObject(typeof(Door.Colors), Int64.Parse(data[3])), data[4]);
                newDoor.gameObject.transform.position = new Vector2(Int64.Parse(data[1]) / tileSize, -Int64.Parse(data[2]) / tileSize);
                doors.Add(newDoor);
                continue;
            }

            Tile newTile = new Tile(Instantiate(tilePrefab), tile.tileType);
            newTile.gameObject.transform.position = new Vector2(Int64.Parse(data[0]) / tileSize, -Int64.Parse(data[1]) / tileSize);

            newTile.tileType = (Tile.Type)Enum.ToObject(typeof(Tile.Type), Int64.Parse(data[2]));

            newTile.UpdateColor();
            tiles.Add(newTile);

        }
        return true;
    }

    public void NewLevelClick()
    {
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }

        if (levelInput.text != "")
        {
            currentLevel = levelInput.text.ToLower().Replace(' ', '-');
            uiVisible = false;
        }
    }

    public void LoadLevelClick(string fileName)
    {
        if (fileName != "")
        {
            if (LoadLevel(fileName.ToLower().Replace(' ', '-')))
            {
                currentLevel = fileName.ToLower().Replace(' ', '-');
                uiVisible = false;
            }
        }
    }

    public void CheckExistingLevels()
    {
        foreach (Button b in buttons)
        {
            Destroy(b.gameObject);
        }

        buttons.Clear();

        int multiplier = 0;
        if (Directory.Exists(directory))
        {
            foreach (String fileName in Directory.EnumerateFiles(directory))
            {
                string s = fileName;
                Button newButton = Instantiate(buttonPrefab);
                newButton.transform.SetParent(loadContent.transform);

                s = fileName.Replace("Levels/", "");
                s = s.Replace(".txt", "");

                newButton.GetComponentInChildren<Text>().text = s;

                newButton.onClick.AddListener((delegate { LoadLevelClick(s); }));

                buttons.Add(newButton);

                multiplier++;
            }
        }
    }

}
