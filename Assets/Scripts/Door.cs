﻿using UnityEngine;

public class Door
{
    public static Sprite doorSprite;
    public static Sprite doorYellowSprite;
    public static Sprite doorGreenSprite;
    public static Sprite doorBlueSprite;
    public static Sprite doorRedSprite;
    public static Sprite doorOrangeSprite;

    public string levelToLoad = "";

    public enum Colors { Gray, Yellow, Green, Blue, Red, Orange, Delete, Construction };

    public Colors doorColor;

    private SpriteRenderer spriteRenderer;

    public GameObject gameObject;

    public Door(GameObject go, Colors doorColor, string level)
    {
        this.doorColor = doorColor;
        levelToLoad = level;
        gameObject = go;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        UpdateColor();
    }

    public void UpdateColor()
    {
        if (this.doorColor == Colors.Gray)
        {
            spriteRenderer.sprite = Door.doorSprite;
        }
        else if (this.doorColor == Colors.Yellow)
        {
            spriteRenderer.sprite = Door.doorYellowSprite;
        }
        else if (this.doorColor == Colors.Green)
        {
            spriteRenderer.sprite = Door.doorGreenSprite;
        }
        else if (this.doorColor == Colors.Blue)
        {
            spriteRenderer.sprite = Door.doorBlueSprite;
        }
        else if (this.doorColor == Colors.Red)
        {
            spriteRenderer.sprite = Door.doorRedSprite;
        }
        else if (this.doorColor == Colors.Orange)
        {
            spriteRenderer.sprite = Door.doorOrangeSprite;
        }
        else if (this.doorColor == Colors.Construction)
        {
            //base.sprite = Tile.constructionTileSprite;
        }
        else if (this.doorColor == Colors.Delete)
        {
            //base.sprite = Tile.deleteTileSprite;
        }
    }

    public override string ToString()
    {
        return "D|" + gameObject.transform.position.x * 64 + "|" + -gameObject.transform.position.y * 64 + "|" + (int)doorColor + "|" + levelToLoad;
    }
}
