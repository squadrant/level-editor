﻿using UnityEngine;

public class Tile
{

    public static Sprite tileSprite;
    public static Sprite tileYellowSprite;
    public static Sprite tileGreenSprite;
    public static Sprite tileBlueSprite;
    public static Sprite tileRedSprite;
    public static Sprite tileOrangeSprite;

    public static Sprite constructionTileSprite;
    public static Sprite deleteTileSprite;

    public enum Type { Gray, Yellow, Green, Blue, Red, Orange, Delete, Construction };

    public Type tileType;

    private SpriteRenderer spriteRenderer;

    public GameObject gameObject;

    public Tile(GameObject go, Type type)
    {
        gameObject = go;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        tileType = type;
    }

    public void UpdateColor()
    {
        if (tileType == Type.Gray)
        {
            spriteRenderer.sprite = Tile.tileSprite;
        }
        else if (tileType == Type.Yellow)
        {
            spriteRenderer.sprite = Tile.tileYellowSprite;
        }
        else if (tileType == Type.Green)
        {
            spriteRenderer.sprite = Tile.tileGreenSprite;
        }
        else if (tileType == Type.Blue)
        {
            spriteRenderer.sprite = Tile.tileBlueSprite;
        }
        else if (tileType == Type.Red)
        {
            spriteRenderer.sprite = Tile.tileRedSprite;
        }
        else if (tileType == Type.Orange)
        {
            spriteRenderer.sprite = Tile.tileOrangeSprite;
        }
        else if (tileType == Type.Construction)
        {
            spriteRenderer.sprite = Tile.constructionTileSprite;
        }
        else if (tileType == Type.Delete)
        {
            spriteRenderer.sprite = Tile.deleteTileSprite;
        }
    }

    public override string ToString()
    {
        return gameObject.transform.position.x * 64 + "|" + -gameObject.transform.position.y * 64 + "|" + (int)tileType;
    }

}
