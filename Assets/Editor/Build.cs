﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class Build : MonoBehaviour
{
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target == BuildTarget.StandaloneOSX)
        {
            BumpBundleVersion();
        }
    }

    [MenuItem("Tools/Build", false, 10)]
    public static void CustomBuild()
    {
        BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, "Build/Win/Level Editor.exe", BuildTarget.StandaloneWindows, BuildOptions.None);
        BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, "Build/Mac/Level Editor.app", BuildTarget.StandaloneOSX, BuildOptions.Development);
    }

    // Bump version number in PlayerSettings.bundleVersion
    private static void BumpBundleVersion()
    {
        float versionFloat;

        if (float.TryParse(PlayerSettings.bundleVersion, out versionFloat))
        {
            versionFloat += 0.01f;
            PlayerSettings.bundleVersion = versionFloat.ToString();
        }
    }
}
